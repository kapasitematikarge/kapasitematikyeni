﻿<head>
<meta name="description" content="KEBA Production Manager Sezer Cintan : “Production is a serious business, if you are doing this job you cannot act with assumptions. If you have dozens of stalls on your course , each of them; you want to know how many parts it takes. KAPASITEMATIK offers the user exactly these opportunities. You can monitor every stage of your production from anywhere with internet reports and intervene where necessary. So you don't leave your job to chance… ”"/>
<meta property="og:description" content="KEBA Production Manager Sezer Cintan : “Production is a serious business, if you are doing this job you cannot act with assumptions. If you have dozens of stalls on your course , each of them; you want to know how many parts it takes. KAPASITEMATIK offers the user exactly these opportunities. You can monitor every stage of your production from anywhere with internet reports and intervene where necessary. So you don't leave your job to chance… ”"/>
<meta name="keywords" content="kapasitematik, production, machining, workbench"/>
<meta property="og:url" content="https://www.kapasitematik.com/referances/you-never-break-in-your-production-area-with-kapasitematik"/>
<meta property="og:title" content="You Never Break In Your Production Area With Kapasitematik"/>
<meta property="og:image" content="http://www.machinetoolexpress.com/resim/c3.png"/>
</head>

<div class="realfactory-page-wrapper" id="realfactory-page-wrapper">
<div class="realfactory-page-title-wrap  realfactory-style-custom realfactory-left-align">
        <div class="realfactory-header-transparent-substitute" style="height: 70px;"></div>
        <div class="realfactory-page-title-overlay"></div>
        <div class="realfactory-page-title-container realfactory-container">
            <div class="realfactory-page-title-content realfactory-item-pdlr">
                <h1 class="realfactory-page-title">You Never Break In Your Production Area With Kapasitematik</h1>
            </div>
        </div>
    </div>
<div class="realfactory-content-container realfactory-container">
<div class=" realfactory-sidebar-wrap clearfix realfactory-line-height-0 realfactory-sidebar-style-right">
<div class=" realfactory-sidebar-center realfactory-column-40 realfactory-line-height">
<div class="realfactory-content-wrap realfactory-item-pdlr clearfix"><div class="realfactory-content-area">
<article id="post-1313" class="post-1313 post type-post status-publish format-standard has-post-thumbnail hentry category-blog tag-metal tag-mining tag-polymer">
<div class="realfactory-single-article">
<div class="realfactory-single-article-content">
<p><strong>KEBA Production Manager Sezer Cintan : “Production is a serious business, if you are doing this job you cannot act with assumptions. If you have dozens of stalls on your course , each of them; you want to know how many parts it takes. KAPASITEMATIK offers the user exactly these opportunities. You can monitor every stage of your production from anywhere with internet reports and intervene where necessary. So you don't leave your job to chance… ”</strong></p>

<p><strong>Can you tell us about your company ?</strong></p>

<p>We started production in 2006. We have added the expert staff of Arslan Oto, which has been a water pump and OEM supplier for more than 40 years, within our organization. We have Keba Otomotiv, Keba Egzoz and Keba Dokum brands within our structure. We carry out production and sales activities with a staff of 70 in an area of 8,000 square meters, of which 4,000 are closed.</p>

<p><strong>Where do you organize export ?</strong></p>

<p>We export 65 percent of our production directly to 42 countries with the Keba brand. OEM and OES customers supply 20 percent.</p>

<p><strong>How do you ensure quality in production ?</strong></p>

<p>In today's economy, everyone has to save money by reducing costs. That's why Keba as a product of high quality to our customers, we are doing our best to be able to offer an affordable price. Inexpensive but poor quality products that are used for the purpose of saving fail quickly to the customer, instead of saving time and more money is lost. And as Keba , we say that quality always wins. ”Our aim is to be in the top 3 in the independent spare parts market in 2023.</p>

<p><strong>Shall we talk about the habits you say “we never give up” in the production ?</strong></p>

<p>First of all, we have an expert, experienced workforce. We, as a company, have brought this experience together with quality. The processes of all Keba branded products from raw material to finished products are carried out under the control of Keba's experienced and expert team. At these stages, all products undergo 100 percent sealing testing. Keba is one of the rare companies with the ability to perform in-house tests such as sealing test and life test at -25 C°, including the simulation of the pump's operation on the engine thanks to the test machines. For this reason, our company pursues continuous process improvement and following and owning the latest technologies.</p>

<p><strong>How did you meet TEZMAKSAN technologies ?</strong></p>

<p>As you can see, our production area is full of TEZMAKSAN brands.Our collaboration started in 2011. Firstly, we bought a Goodway GLS 2000 from Tezmaksan. Now, we have 10 machines including Finetech, Goodway and Brother from Tezmaksan. In any matter, we can solve problems with our interlocutors we contact from Tezmaksan.</p>

<p><strong>A whole new application.How did you get convinced ?</strong></p>

<p>We have 10 machines in our production area from Tezmaksan. This system provided us with the opportunity to follow all machines with the presentation and the examples shown.It was very tempting to get the data from the machines in a variety of formats. As a result, Keba is very open to technology and development.</p>

<p><strong>What did Kapasitematik bring to your production area? Are you satisfied ?</strong></p>

<p>Production is a serious business, and if you're doing it, you can't act hypothetically. The operators who use them are also not robots, but there can always be a margin of error or error. It is very important to get to the clearest data and that is what capacitance gives the user. You can monitor which parts of the machines, how long and how many are processed. What is in the production phase, what should be, what should not be is displayed in detailed reports. You can intervene instantly when it shouldn't be. You can review these reports and monitor the data from any internet environment. Naturally, with this application, you do not go away from production and you are always in the factory.</p>

<p><strong>Why did you choose Tezmaksan ?</strong></p>

<p>I mentioned Tezmaksan's customer-oriented work. However, we would also like to express the professionalism of the firm. Because after sales and services are very strong.The adaptation of the machine and operator is very important to avoid wasting time in production and compromising quality.TEZMAKSAN follows the machine that it sells and approaches the installation-training issue very meticulous.</p>

<p><strong>Are you having Tezmaksan do the planned maintenance ?</strong></p>

<p>Tezmaksan is responsible for the maintenance of the machines. Rather, they took on this responsibility themselves. We have scheduled maintenance periodically. I've expressed our sensitivity to quality. Planned maintenance is also a key application for quality, precision and time. Production Managers don't like surprises. With planned maintenance, bad surprises disappear.</p>

<p><strong>How did you meet Kapasitematik ?</strong></p>

<p>We've been working within ourselves for three years while we've been thinking about quality. During the planning phase, TEZMAKSAN Aegean Regional Director Asim Cetin visited us. He talked about Tezmaksan's own engineering software application, Kapasitematik. When they say "quality, efficiency and time saving", we immediately approach the subject with excitement.</p>
</div></div></div></div></div></div>


