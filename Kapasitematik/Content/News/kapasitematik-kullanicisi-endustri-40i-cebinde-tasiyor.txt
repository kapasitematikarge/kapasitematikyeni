﻿<div class="realfactory-page-wrapper" id="realfactory-page-wrapper">
<div class="realfactory-page-title-wrap  realfactory-style-custom realfactory-left-align">
        <div class="realfactory-header-transparent-substitute" style="height: 70px;"></div>
        <div class="realfactory-page-title-overlay"></div>
        <div class="realfactory-page-title-container realfactory-container">
            <div class="realfactory-page-title-content realfactory-item-pdlr">
                <h1 class="realfactory-page-title">KAPASİTEMATİK kullanıcısı, Endüstri 4.0’ı cebinde taşıyor</h1>
            </div>
        </div>
    </div>
<div class="realfactory-content-container realfactory-container">
<div class=" realfactory-sidebar-wrap clearfix realfactory-line-height-0 realfactory-sidebar-style-right">
<div class=" realfactory-sidebar-center realfactory-column-40 realfactory-line-height">
<div class="realfactory-content-wrap realfactory-item-pdlr clearfix"><div class="realfactory-content-area">
<article id="post-1313" class="post-1313 post type-post status-publish format-standard has-post-thumbnail hentry category-blog tag-metal tag-mining tag-polymer">
<div class="realfactory-single-article">
<div class="realfactory-single-article-content">
 <p>
    <strong>KAPASİTEMATİK kullanıcısı, Endüstri 4.0’ı cebinde taşıyor&nbsp;</strong>
  </p>

  <p>TEZMAKSAN’ın çağın yeni sanayi devrimi Endüstri 4.0 kapsamında geliştirdiği yazılım projesi olan KAPASİTEMATİK, içerdiği data takip ve raporlama sistemi ile kullanıcısına mucizeler yaratmaya devam ediyor. Ayrıca TEZMAKSAN Ar-Ge ve İleri Teknoloji Departmanı, kendi geliştirdiği yazılıma kattığı uygulamalar ile KAPASİTEMATİK’e yenilikler kazandırarak kullanıcıyı daha da avantajlı kılıyor.</p>

  <p>
    <strong>Duymayanlar için KAPASİTEMATİK&nbsp;</strong>
  </p>

  <p>TEZMAKSAN Makine, Ar-Ge ve İleri Teknoloji Departmanı’nın geliştirdiği yazılım programı KAPASİTEMATİK ile işletme sahibi kullanıcılar, internet olan her ortamda makinelerini ve üretim performansını takip edebiliyor. “Makina verimli çalışıyor mu? Ne zaman arıza verdi? Operatör tarafından doğru program kullanılıyor mu” gibi bilgiler, anında tablet veya cep telefonundan kontrol edilebiliyor ve kullanıcı, üretimini anbean izleyebiliyor. Bu sayede işletmeler hem zamandan hem maliyetten ciddi tasarruf sağlıyor.&nbsp;</p>

  <p>
    <strong>Kullanıcıya avantaj sağlayan yeni sekmeler&nbsp;</strong>
  </p>

  <p>TEZMAKSAN Ar-Ge ve İleri Teknoloji Departmanı, KAPASİTEMATİK’in içerdiği performans ve raporlar sekmeleri içinde alt birimler oluşturarak müşteriye yeni özellikler sundu. Bu şekilde performansın, basit ve kolay analizlerinin yapılması mümkün oldu. Bu yeni özellikler: Makine performansı, üretim performansı, operatör performansı olarak kullanıcıya anlık olarak sunulabilmektedir. &nbsp;Müşterinin teknolojinin mevcut durumunu takip edebilmesi ve işletmesinde süreklilik sağlayabilmesi için bu yeni özellikler, KAPASİTEMATİK’e kazandırılmıştır.</p>

  <p>
    <strong>Raporlara takip kolaylığı geldi&nbsp;</strong>
  </p>

  <p>“Raporlar” sekmesinin alt birimleri, “Makine Raporları” ve “Üretim Raporları” olarak iki bölümden oluşmaktadır. “Makine Raporları” sekmesi, belirlediğiniz tarihler skalasında geriye dönük makine performansını, internet erişimi olan her yerde raporları inceleyebilme, tarihler arası kıyaslama yapma imkanı veriyor. “Üretim Raporları” sekmesiyle; önceden toplanmış üretime dair raporları tek tek inceleyip analiz ile uğraşmak yerine; istediğiniz zaman aralıklarını anında raporlayıp üretiminizi hızlıca planlama şansına sahipsiniz. Zamandan kazanım rekabetin yoğun yaşandığı sektörümüzde önemli bir unsur olduğundan, KAPASİTEMATİK’in bu yeni sekmesi sayesinde kullanıcı zamandan kazanacak, işletmenin sürekliliği daha da sağlıklı bir hale gelecek.&nbsp;</p>

  <p>
    <strong>Üretim dataları cebinizde&nbsp;</strong>
  </p>

  <p>Gelişen teknoloji ve internet hızı sayesinde bilgisayarlar, akıllı telefonlar aracılığıyla artık cebimizde… KAPASİTEMATİK kullanıcısı da makinesi ve performansıyla ilgili dataya cep telefonu, tablet bilgisayar, web üzerinden anında erişip üretimine yön verebilmektedir.&nbsp;</p>

  <p>
    <strong>Operatör müdahalesi ortadan kalkacak&nbsp;</strong>
  </p>

  <p>Ayrıca TEZMAKSAN Ar-Ge ve İleri Teknoloji Departmanı, operatör müdahalesi gerektiren durumlarda &nbsp;KAPASİTEMATİK yazılımının operatörden bağımsız biçimde makinaya komut yönlendirmesi üzerinde çalışıyor. Bu yenilik ile operatör müdahalesi gerektiren set up, takım değiştirme, mola vb. aşamalar KAPASİTEMATİK sayesinde operatörsüz gerçekleşebilecek. &nbsp;KAPASİTEMATİK, makinaya komut verebilir hale gelecek ve çeşitlilik gösteren birçok duruşun “nedenler” olarak girip, raporlama sistemine kaydedebilme seçeneği eklenecek.&nbsp;</p>

  <p>&nbsp;</p>
</span>



<p></p>
</div></div></div></div></div></div>