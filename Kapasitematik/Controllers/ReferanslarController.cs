﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Resources;
using System.Threading.Tasks;
using Kapasitematik.Business;
using Kapasitematik.Models;
using Microsoft.AspNetCore.Mvc;

namespace Kapasitematik.Controllers
{
    public class ReferanslarController : Controller
    {

        [Route("Referanslar")]
        public IActionResult ReferansList()
        {
            ReferanceManager referanceManager = new ReferanceManager();

            var referanceFiltered = referanceManager.GetReferanceFromJson(MyServer.MapPath("content/news/referanslar.json"));

            return View(referanceFiltered);

        }

        [Route("Referances")]
        public IActionResult ReferansListEn()
        {
            ReferanceManager referanceManager = new ReferanceManager();

            var referanceFiltered = referanceManager.GetReferanceFromJson(MyServer.MapPath("content/news/referanslaren.json"));

            return View(referanceFiltered);

        }

        [Route("Referanslar/{referanstitle}")]
        public IActionResult ReferansPanel(string referanstitle)
        {
            Referances referances = new Referances();
            string newsTitleFormatted = referanstitle.Replace("-", "_").ToLower();

            ViewBag.Title = referanstitle.Replace("-", " ");

            ReferanceManager referanceManager = new ReferanceManager();

            string sourceRaw = referanceManager.GetReferanceDetailFromFile(MyServer.MapPath("content/News/" + referanstitle + ".txt"));

            if (!string.IsNullOrEmpty(sourceRaw))
            {
                referances.Content = sourceRaw;
            }
            else
            {
                referances.Content = "Bu içerik bulunmamaktadır. Lütfen adresin doğru yazıldığından emin olun.";
            }

            referances.Title = referanstitle.Replace("-", " ").ToUpper();

            return View(referances);
        }

        [Route("Referances/{referanstitle}")]
        public IActionResult ReferancePanel(string referanstitle)
        {
            Referances referances = new Referances();
            string newsTitleFormatted = referanstitle.Replace("-", "_").ToLower();

            ViewBag.Title = referanstitle.Replace("-", " ");

            ReferanceManager referanceManager = new ReferanceManager();

            string sourceRaw = referanceManager.GetReferanceDetailFromFile(MyServer.MapPath("content/News/" + referanstitle + ".txt"));

            if (!string.IsNullOrEmpty(sourceRaw))
            {
                referances.Content = sourceRaw;
            }
            else
            {
                referances.Content = "Bu içerik bulunmamaktadır. Lütfen adresin doğru yazıldığından emin olun.";
            }

            referances.Title = referanstitle.Replace("-", " ").ToUpper();

            return View(referances);
        }
        private static string GetResourceTitle<T>(string key)
        {
            ResourceManager rm = new ResourceManager(typeof(T));
            string someString = rm.GetString(key);
            return someString;
        }
    }
}
