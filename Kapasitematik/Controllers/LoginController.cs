﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Kapasitematik.Database;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using System.Security.Claims;
using Microsoft.AspNetCore.Authentication;

namespace Kapasitematik.Controllers
{
    public class LoginController : Controller
    {
        [Route("login")]
        public IActionResult Index()
        {
            return View();
        }

        [ValidateAntiForgeryToken]
        public IActionResult Token(User User)
        {
            if (ModelState.IsValid)
            {
                using (KpstmtkSiteContext db = new KpstmtkSiteContext())
                {
                    var obj = db.User.Where(a => a.Username.Equals(User.Username) && a.Password.Equals(User.Password)).FirstOrDefault();
                    if (obj != null)
                    {
                        HttpContext.Session.SetString("UserID", obj.Id.ToString());
                        HttpContext.Session.SetString("FirstName", obj.Name.ToString() + " " + obj.Surname.ToString());
                        return RedirectToAction("Index", "Admin");
                    }
                    else
                    {
                        TempData["mesaj"] = "Kullanıcı Adı veya Şifre Hatalı!";// mesaj TempData içerisinde saklanır
                    }
                }
            }
            return RedirectToAction("Index", "Login");
        }

        public IActionResult Logout()
        {
            HttpContext.Session.Clear();
            return RedirectToAction("Index", "Login");
        }
    }
}

