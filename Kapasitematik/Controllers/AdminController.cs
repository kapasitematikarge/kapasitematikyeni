﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using System.Web.Helpers;
using Kapasitematik.Business;
using Kapasitematik.Database;
using Kapasitematik.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Kapasitematik.Controllers
{
    [ControlLogin]
    public class AdminController : Controller
    {
        private readonly KpstmtkSiteContext _dbContext;
        private readonly IWebHostEnvironment _hostEnvironment;

        public AdminController(KpstmtkSiteContext context, IWebHostEnvironment hostEnvironment)
        {
            _dbContext = context;
            this._hostEnvironment = hostEnvironment;
        }

        public IActionResult Index()
        {
            // GET: Admin
            ViewBag.Name = HttpContext.Session.GetString("FirstName");
            ViewBag.YorumOnay = _dbContext.Yorum.Where(x=>x.Onay == false).Count();
            ViewBag.BlogSay = _dbContext.Article.Count();
            ViewBag.YorumSay = _dbContext.Yorum.Count();
            return View();
        }

        [Route("makale-listesi")]
        public IActionResult ArticleList()
        {
            ViewBag.Name = HttpContext.Session.GetString("FirstName");
            ViewBag.YorumOnay = _dbContext.Yorum.Where(x => x.Onay == false).Count();
            return View();
        }

        [Route("ing-makale-listesi")]
        public IActionResult ArticleListEn()
        {
            ViewBag.Name = HttpContext.Session.GetString("FirstName");
            ViewBag.YorumOnay = _dbContext.Yorum.Where(x => x.Onay == false).Count();
            return View();
        }

        public JsonResult GetArticle()
        {
            var userId = HttpContext.Session.GetString("UserID");
            var uid = Convert.ToInt32(userId);
            var query = from m in _dbContext.Article
                        where m.FkUserId == uid
                        orderby m.Id descending
                        select new { Id = m.Id, Baslik = m.Baslik, Icerik = m.Icerik, ResimURL = m.ResimUrl, Tarih = m.Tarih };
            var articles = query.ToList().Select(r => new Article
            {
                Id = r.Id,
                Baslik = r.Baslik,
                Icerik = r.Icerik,
                ResimUrl = r.ResimURL,
                Tarih = Convert.ToDateTime(r.Tarih.ToShortDateString())
            }).ToList();

            return Json(articles);
        }

        public JsonResult GetArticleEn()
        {
            var userId = HttpContext.Session.GetString("UserID");
            var uid = Convert.ToInt32(userId);
            var query = from m in _dbContext.ArticleEn
                        where m.FkUserId == uid
                        orderby m.Id descending
                        select new { Id = m.Id, Baslik = m.Baslik, Icerik = m.Icerik, ResimURL = m.ResimUrl, Tarih = m.Tarih };
            var articles = query.ToList().Select(r => new ArticleEn
            {
                Id = r.Id,
                Baslik = r.Baslik,
                Icerik = r.Icerik,
                ResimUrl = r.ResimURL,
                Tarih = Convert.ToDateTime(r.Tarih.ToShortDateString())
            }).ToList();

            return Json(articles);
        }

        [HttpGet]
        [Route("makale-ekle")]
        public IActionResult AddArticle()
        {
            ViewBag.Name = HttpContext.Session.GetString("FirstName");
            ViewBag.YorumOnay = _dbContext.Yorum.Where(x => x.Onay == false).Count();
            return View();
        }

        [HttpPost]
        [Route("makale-ekle")]
        public IActionResult AddArticle(Article article, IFormFile ResimURL)
        {
            var userId = HttpContext.Session.GetString("UserID");
            var uid = Convert.ToInt32(userId);
            if (ResimURL != null)
            {
                string uploadDir = Path.Combine(_hostEnvironment.WebRootPath, "Uploads");
                string fileName = ResimURL.FileName;
                string filePath = Path.Combine(uploadDir, fileName);
                string folderName = "/Uploads";
                string extention = ResimURL.ContentType.Split("/")[1];
                string fullPath = Path.Combine(uploadDir, fileName);
                string envpath = folderName + "/" + fileName;
                using (var stream = new FileStream(fullPath, FileMode.Create))
                {
                    ResimURL.CopyTo(stream);
                }
                article.ResimUrl = envpath;
                article.FkUserId = uid;
                article.OkunmaSayisi = 0;
                _dbContext.Article.Add(article);
                _dbContext.SaveChanges();
            }
            else
            {
                TempData["mesaj"] = "Hata oluştu! Tekrar Deneyiniz.";// mesaj TempData içerisinde saklanır
            }
            return RedirectToAction("AddArticle","Admin");
        }

        [HttpGet]
        [Route("ing-makale-ekle")]
        public IActionResult AddArticleEn()
        {
            ViewBag.Name = HttpContext.Session.GetString("FirstName");
            ViewBag.YorumOnay = _dbContext.Yorum.Where(x => x.Onay == false).Count();
            return View();
        }

        [HttpPost]
        [Route("ing-makale-ekle")]
        public IActionResult AddArticleEn(ArticleEn article, IFormFile ResimURL)
        {
            var userId = HttpContext.Session.GetString("UserID");
            var uid = Convert.ToInt32(userId);
            if (ResimURL != null)
            {
                string uploadDir = Path.Combine(_hostEnvironment.WebRootPath, "Uploads");
                string fileName = ResimURL.FileName;
                string filePath = Path.Combine(uploadDir, fileName);
                string folderName = "/Uploads";
                string extention = ResimURL.ContentType.Split("/")[1];
                string fullPath = Path.Combine(uploadDir, fileName);
                string envpath = folderName + "/" + fileName;
                using (var stream = new FileStream(fullPath, FileMode.Create))
                {
                    ResimURL.CopyTo(stream);
                }
                article.ResimUrl = envpath;
                article.FkUserId = uid;
                article.OkunmaSayisi = 0;
                _dbContext.ArticleEn.Add(article);
                _dbContext.SaveChanges();
            }
            else
            {
                TempData["mesaj"] = "Hata oluştu! Tekrar Deneyiniz.";// mesaj TempData içerisinde saklanır
            }
            return RedirectToAction("AddArticleEn", "Admin");
        }

        public IActionResult EditArticle(int id)
        {
            ViewBag.Name = HttpContext.Session.GetString("FirstName");
            ViewBag.YorumOnay = _dbContext.Yorum.Where(x => x.Onay == false).Count();
            var blog = _dbContext.Article.Where(x => x.Id == id).SingleOrDefault();
            return View(blog);
        }

        [HttpPost]
        public IActionResult EditArticle(int id, Article article, IFormFile ResimURL)
        {
            var userId = HttpContext.Session.GetString("UserID");
            var uid = Convert.ToInt32(userId);

            if (ModelState.IsValid)
            {
                var blog = _dbContext.Article.Where(x => x.Id == id).SingleOrDefault();
                string envpath = null;
                if (ResimURL != null)
                {
                    if (System.IO.File.Exists(MyServer.MapPath(blog.ResimUrl)))
                    {
                        System.IO.File.Delete(MyServer.MapPath(blog.ResimUrl));
                    }

                    string uploadDir = Path.Combine(_hostEnvironment.WebRootPath, "Uploads");
                    string fileName = ResimURL.FileName;
                    string filePath = Path.Combine(uploadDir, fileName);
                    string folderName = "/Uploads";
                    string extention = ResimURL.ContentType.Split("/")[1];
                    string fullPath = Path.Combine(uploadDir, fileName);
                    envpath = folderName + "/" + fileName;
                    using (var stream = new FileStream(fullPath, FileMode.Create))
                    {
                        ResimURL.CopyTo(stream);
                    }

                    article.FkUserId = uid;
                    article.ResimUrl = envpath;
                }
                blog.Baslik = article.Baslik;
                blog.Icerik = article.Icerik;
                blog.Tarih = article.Tarih;
                blog.ResimUrl = envpath;
                _dbContext.SaveChanges();
                return RedirectToAction("ArticleList");
            }
            else
            {
                TempData["mesaj"] = "Hata oluştu! Tekrar Deneyiniz.";// mesaj TempData içerisinde saklanır
            }
            return View(article);
        }

        public IActionResult EditArticleEn(int id)
        {
            ViewBag.Name = HttpContext.Session.GetString("FirstName");
            ViewBag.YorumOnay = _dbContext.Yorum.Where(x => x.Onay == false).Count();
            var blog = _dbContext.ArticleEn.Where(x => x.Id == id).SingleOrDefault();
            return View(blog);
        }

        [HttpPost]
        public IActionResult EditArticleEn(int id, ArticleEn article, IFormFile ResimURL)
        {
            var userId = HttpContext.Session.GetString("UserID");
            var uid = Convert.ToInt32(userId);

            if (ModelState.IsValid)
            {
                var blog = _dbContext.ArticleEn.Where(x => x.Id == id).SingleOrDefault();
                string envpath = null;
                if (ResimURL != null)
                {
                    if (System.IO.File.Exists(MyServer.MapPath(blog.ResimUrl)))
                    {
                        System.IO.File.Delete(MyServer.MapPath(blog.ResimUrl));
                    }

                    string uploadDir = Path.Combine(_hostEnvironment.WebRootPath, "Uploads");
                    string fileName = ResimURL.FileName;
                    string filePath = Path.Combine(uploadDir, fileName);
                    string folderName = "/Uploads";
                    string extention = ResimURL.ContentType.Split("/")[1];
                    string fullPath = Path.Combine(uploadDir, fileName);
                    envpath = folderName + "/" + fileName;
                    using (var stream = new FileStream(fullPath, FileMode.Create))
                    {
                        ResimURL.CopyTo(stream);
                    }

                    article.FkUserId = uid;
                    article.ResimUrl = envpath;
                }
                blog.Baslik = article.Baslik;
                blog.Icerik = article.Icerik;
                blog.Tarih = article.Tarih;
                blog.ResimUrl = envpath;
                _dbContext.SaveChanges();
                return RedirectToAction("ArticleListEn");
            }
            else
            {
                TempData["mesaj"] = "Hata oluştu! Tekrar Deneyiniz.";// mesaj TempData içerisinde saklanır
            }
            return View(article);
        }

        public IActionResult DeleteArticle(int id)
        {
            var blog = _dbContext.Article.Find(id);
            _dbContext.Article.Remove(blog);
            _dbContext.SaveChanges();
            return RedirectToAction("ArticleList");
        }

        public IActionResult DeleteArticleEn(int id)
        {
            var blog = _dbContext.ArticleEn.Find(id);
            _dbContext.ArticleEn.Remove(blog);
            _dbContext.SaveChanges();
            return RedirectToAction("ArticleListEn");
        }
    }
}
