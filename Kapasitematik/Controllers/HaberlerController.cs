﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Resources;
using System.Threading.Tasks;
using Kapasitematik.Business;
using Kapasitematik.Models;
using Microsoft.AspNetCore.Mvc;

namespace Kapasitematik.Controllers
{
    public class HaberlerController : Controller
    {
        [Route("Haberler")]
        public IActionResult NewsList()
        {
            NewsManager newsManager = new NewsManager();

            var newsFiltered = newsManager.GetNewsFromJson(MyServer.MapPath("Content/news/haberler.json"));

            return View(newsFiltered);

        }

        [Route("Haberler/{newstitle}")]
        public IActionResult NewsPanel(string newstitle)
        {
            News news = new News();
            string newsTitleFormatted = newstitle.Replace("-", "_").ToLower();

            ViewBag.Title = newstitle.Replace("-", " ");

            NewsManager newsManager = new NewsManager();

            string sourceRaw = newsManager.GetNewsDetailFromFile(MyServer.MapPath("Content/News/" + newstitle + ".txt"));

            if (!string.IsNullOrEmpty(sourceRaw))
            {
                news.Content = sourceRaw;
            }
            else
            {
                news.Content = "Bu içerik bulunmamaktadır. Lütfen adresin doğru yazıldığından emin olun.";
            }

            news.Title = newstitle.Replace("-", " ").ToUpper();

            return View(news);
        }
        private static string GetResourceTitle<T>(string key)
        {
            ResourceManager rm = new ResourceManager(typeof(T));
            string someString = rm.GetString(key);
            return someString;
        }
    }
}
