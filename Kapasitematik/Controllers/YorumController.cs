﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Kapasitematik.Database;
using Microsoft.AspNetCore.Http;
using Kapasitematik.Models;

namespace Kapasitematik.Controllers
{
    [ControlLogin]
    public class YorumController : Controller
    {
        private readonly KpstmtkSiteContext _context;

        public YorumController(KpstmtkSiteContext context)
        {
            _context = context;
        }

        // GET: Yorum
        [Route("yorum")]
        public async Task<IActionResult> Index()
        {
            ViewBag.Name = HttpContext.Session.GetString("FirstName");
            ViewBag.YorumOnay = _context.Yorum.Where(x => x.Onay == false).Count();
            var kpstmtkSiteContext = _context.Yorum.Include(y => y.FkArticle);
            return View(await kpstmtkSiteContext.ToListAsync());
        }

        // GET: Yorum/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var yorum = await _context.Yorum
                .Include(y => y.FkArticle)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (yorum == null)
            {
                return NotFound();
            }

            return View(yorum);
        }

        // GET: Yorum/Create
        public IActionResult Create()
        {
            ViewBag.Name = HttpContext.Session.GetString("FirstName");
            ViewBag.YorumOnay = _context.Yorum.Where(x => x.Onay == false).Count();
            ViewData["FkArticleId"] = new SelectList(_context.Article, "Id", "Baslik");
            return View();
        }

        // POST: Yorum/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Icerik,AdSoyad,Eposta,Onay,FkUserId,FkArticleId")] Yorum yorum)
        {
            if (ModelState.IsValid)
            {
                _context.Add(yorum);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            else
            {
                TempData["mesaj"] = "Hata oluştu! Tekrar Deneyiniz.";// mesaj TempData içerisinde saklanır
            }
            ViewData["FkArticleId"] = new SelectList(_context.Article, "Id", "Baslik", yorum.FkArticleId);
            return View(yorum);
        }

        // GET: Yorum/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            ViewBag.Name = HttpContext.Session.GetString("FirstName");
            ViewBag.YorumOnay = _context.Yorum.Where(x => x.Onay == false).Count();
            if (id == null)
            {
                return NotFound();
            }

            var yorum = await _context.Yorum.FindAsync(id);
            if (yorum == null)
            {
                return NotFound();
            }
            ViewData["FkArticleId"] = new SelectList(_context.Article, "Id", "Baslik", yorum.FkArticleId);
            return View(yorum);
        }

        // POST: Yorum/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Icerik,AdSoyad,Eposta,Onay,FkUserId,FkArticleId")] Yorum yorum)
        {
            if (id != yorum.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(yorum);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!YorumExists(yorum.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        //throw;
                        TempData["mesaj"] = "Hata oluştu! Tekrar Deneyiniz.";// mesaj TempData içerisinde saklanır
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            else
            {
                TempData["mesaj"] = "Hata oluştu! Tekrar Deneyiniz.";// mesaj TempData içerisinde saklanır
            }
            ViewData["FkArticleId"] = new SelectList(_context.Article, "Id", "Baslik", yorum.FkArticleId);
            return View(yorum);
        }

        public IActionResult DeleteYorum(int id)
        {
            var blog = _context.Yorum.Find(id);
            _context.Yorum.Remove(blog);
            _context.SaveChanges();
            return RedirectToAction(nameof(Index));
        }

        private bool YorumExists(int id)
        {
            return _context.Yorum.Any(e => e.Id == id);
        }
    }
}
