﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Kapasitematik.Models
{
    public partial class Mail
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Message { get; set; }
        public Nullable<System.DateTime> CreateDate { get; set; }
        public string Kvkk_Onay { get; set; }
    }
}
