﻿using Kapasitematik.Database;
using PagedList.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Kapasitematik.Models
{
    public class ViewModel
    {
        public IEnumerable<Kapasitematik.Database.Yorum> Yorum { get; set; }
        public IEnumerable<Kapasitematik.Database.Article> Articles { get; set; }
        public IEnumerable<Kapasitematik.Database.Etiket> Etiket { get; set; }
        public Kapasitematik.Database.Article Makale { get; set; }
        public PagedList<Article> Article { get; set; }

        public IEnumerable<Kapasitematik.Database.Article> MyArticle { get; set; }
        public PagedList<Article> OneArticle { get; set; }
        public PagedList<Article> TwoArticle { get; set; }
        public PagedList<Article> ArticleSkipTwo { get; set; }
    }
}
