﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;

namespace Kapasitematik.Database
{
    public partial class Article
    {
        public Article()
        {
            EtiketArticle = new HashSet<EtiketArticle>();
            Yorum = new HashSet<Yorum>();
        }

        public int Id { get; set; }
        public string Baslik { get; set; }
        public string Icerik { get; set; }
        public string KategoriAdi { get; set; }
        public string ResimUrl { get; set; }
        public int? OkunmaSayisi { get; set; }
        public int FkUserId { get; set; }
        public DateTime Tarih { get; set; }

        public virtual User FkUser { get; set; }
        public virtual ICollection<EtiketArticle> EtiketArticle { get; set; }
        public virtual ICollection<Yorum> Yorum { get; set; }
    }
}
