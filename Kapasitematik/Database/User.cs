﻿using System;
using System.Collections.Generic;

namespace Kapasitematik.Database
{
    public partial class User
    {
        public User()
        {
            Article = new HashSet<Article>();
            ArticleEn = new HashSet<ArticleEn>();
        }

        public int Id { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }

        public virtual ICollection<Article> Article { get; set; }
        public virtual ICollection<ArticleEn> ArticleEn { get; set; }
    }
}
