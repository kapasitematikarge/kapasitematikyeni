﻿using System;
using System.Collections.Generic;

namespace Kapasitematik.Database
{
    public partial class EtiketArticle
    {
        public int ArticleId { get; set; }
        public int EtiketId { get; set; }

        public virtual Article Article { get; set; }
        public virtual Etiket Etiket { get; set; }
    }
}
