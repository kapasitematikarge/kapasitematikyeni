﻿using System;
using System.Collections.Generic;

namespace Kapasitematik.Database
{
    public partial class Etiket
    {
        public Etiket()
        {
            EtiketArticle = new HashSet<EtiketArticle>();
        }

        public int Id { get; set; }
        public string EtiketAd { get; set; }

        public virtual ICollection<EtiketArticle> EtiketArticle { get; set; }
    }
}
