﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Kapasitematik.Database
{
    public partial class ArticleEn
    {
        public int Id { get; set; }
        public string Baslik { get; set; }
        public string Icerik { get; set; }
        public string KategoriAdi { get; set; }
        public string ResimUrl { get; set; }
        public int? OkunmaSayisi { get; set; }
        public int FkUserId { get; set; }
        public DateTime Tarih { get; set; }

        public virtual User FkUser { get; set; }
    }
}
