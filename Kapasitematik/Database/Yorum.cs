﻿using System;
using System.Collections.Generic;

namespace Kapasitematik.Database
{
    public partial class Yorum
    {
        public int Id { get; set; }
        public string Icerik { get; set; }
        public string AdSoyad { get; set; }
        public string Eposta { get; set; }
        public bool Onay { get; set; }
        public int FkUserId { get; set; }
        public int FkArticleId { get; set; }

        public virtual Article FkArticle { get; set; }
    }
}
