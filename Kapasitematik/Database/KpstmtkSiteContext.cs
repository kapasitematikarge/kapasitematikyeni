﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Kapasitematik.Database
{
    public partial class KpstmtkSiteContext : DbContext
    {
        public KpstmtkSiteContext()
        {
        }

        public KpstmtkSiteContext(DbContextOptions<KpstmtkSiteContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Article> Article { get; set; }
        public virtual DbSet<ArticleEn> ArticleEn { get; set; }
        public virtual DbSet<Etiket> Etiket { get; set; }
        public virtual DbSet<EtiketArticle> EtiketArticle { get; set; }
        public virtual DbSet<User> User { get; set; }
        public virtual DbSet<Yorum> Yorum { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("Data Source=192.168.88.48;Initial Catalog=KpstmtkSite;User ID=bigdata;Password=zxcvasdf;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Article>(entity =>
            {
                entity.Property(e => e.Baslik)
                    .IsRequired()
                    .HasMaxLength(150);

                entity.Property(e => e.FkUserId).HasColumnName("FkUserID");

                entity.Property(e => e.Icerik).IsRequired();

                entity.Property(e => e.KategoriAdi)
                    .IsRequired()
                    .HasMaxLength(150);

                entity.Property(e => e.ResimUrl)
                    .HasColumnName("ResimURL")
                    .HasMaxLength(250);

                entity.Property(e => e.Tarih)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.HasOne(d => d.FkUser)
                    .WithMany(p => p.Article)
                    .HasForeignKey(d => d.FkUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Article_User");
            });

            modelBuilder.Entity<ArticleEn>(entity =>
            {
                entity.Property(e => e.Baslik)
                    .IsRequired()
                    .HasMaxLength(150);

                entity.Property(e => e.FkUserId).HasColumnName("FkUserID");

                entity.Property(e => e.Icerik).IsRequired();

                entity.Property(e => e.KategoriAdi)
                    .IsRequired()
                    .HasMaxLength(150);

                entity.Property(e => e.ResimUrl)
                    .HasColumnName("ResimURL")
                    .HasMaxLength(250);

                entity.Property(e => e.Tarih)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.HasOne(d => d.FkUser)
                    .WithMany(p => p.ArticleEn)
                    .HasForeignKey(d => d.FkUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_User_ArticleEn");
            });

            modelBuilder.Entity<Etiket>(entity =>
            {
                entity.Property(e => e.EtiketAd)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<EtiketArticle>(entity =>
            {
                entity.HasKey(e => new { e.ArticleId, e.EtiketId });

                entity.HasOne(d => d.Article)
                    .WithMany(p => p.EtiketArticle)
                    .HasForeignKey(d => d.ArticleId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_EtiketArticle_Article");

                entity.HasOne(d => d.Etiket)
                    .WithMany(p => p.EtiketArticle)
                    .HasForeignKey(d => d.EtiketId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_EtiketArticle_Etiket");
            });

            modelBuilder.Entity<User>(entity =>
            {
                entity.Property(e => e.Name).HasMaxLength(50);

                entity.Property(e => e.Password)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Surname).HasMaxLength(50);

                entity.Property(e => e.Username)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<Yorum>(entity =>
            {
                entity.Property(e => e.AdSoyad)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Eposta)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.FkArticleId).HasColumnName("FkArticleID");

                entity.Property(e => e.FkUserId).HasColumnName("FkUserID");

                entity.Property(e => e.Icerik).IsRequired();

                entity.HasOne(d => d.FkArticle)
                    .WithMany(p => p.Yorum)
                    .HasForeignKey(d => d.FkArticleId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Yorum_Article");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
