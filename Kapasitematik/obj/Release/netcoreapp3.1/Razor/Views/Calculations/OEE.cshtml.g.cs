#pragma checksum "C:\Users\emre.senturk\source\repos\Kapasitematik\Kapasitematik\Views\Calculations\OEE.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "48c7ef2abe1b2d53f94ff28c0ed0e4cf3a1f869c"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Calculations_OEE), @"mvc.1.0.view", @"/Views/Calculations/OEE.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "C:\Users\emre.senturk\source\repos\Kapasitematik\Kapasitematik\Views\_ViewImports.cshtml"
using Kapasitematik;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\emre.senturk\source\repos\Kapasitematik\Kapasitematik\Views\_ViewImports.cshtml"
using Kapasitematik.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"48c7ef2abe1b2d53f94ff28c0ed0e4cf3a1f869c", @"/Views/Calculations/OEE.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"b047e735b4ff49080d6105bca84291c8dca4e7ba", @"/_ViewImports.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"19ff504f5cbbc4178e156920e4ecf9aef42e51e7", @"/Views/_ViewImports.cshtml")]
    public class Views_Calculations_OEE : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral("\r\n");
#nullable restore
#line 2 "C:\Users\emre.senturk\source\repos\Kapasitematik\Kapasitematik\Views\Calculations\OEE.cshtml"
  
    ViewData["Title"] = "OEE";
    Layout = "~/Views/Shared/_Layout.cshtml";

#line default
#line hidden
#nullable disable
            WriteLiteral(@"
<style>
    input[type=""number""]::-webkit-outer-spin-button, input[type=""number""]::-webkit-inner-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }

    input[type=""number""] {
        -moz-appearance: textfield;
    }
</style>

<div class=""realfactory-page-wrapper"" id=""realfactory-page-wrapper"">
    <div class=""realfactory-blog-title-wrap  realfactory-style-custom realfactory-feature-image"" style=""background-image: url() ;"">
        <div class=""realfactory-header-transparent-substitute"" style=""height: 70px;""></div>
        <div class=""realfactory-blog-title-overlay""></div>
        <div class=""realfactory-blog-title-container realfactory-container"">
            <div class=""realfactory-blog-title-content realfactory-item-pdlr"" style=""padding-top: 400px ;padding-bottom: 80px ;"">
                <header class=""realfactory-single-article-head clearfix"">
                    <div class=""realfactory-single-article-head-right"">
                        <h1 class=""realfactory-s");
            WriteLiteral(@"ingle-article-title"">Toplam Ekipman Etkinliği (OEE) Hesaplama</h1>
                    </div>
                </header>
            </div>
        </div>
    </div>
    <div class=""realfactory-content-container realfactory-container"">
        <div class="" realfactory-sidebar-wrap clearfix realfactory-line-height-0 realfactory-sidebar-style-right"">
            <div class="" realfactory-sidebar-center realfactory-column-40 realfactory-line-height"">
                <div class=""realfactory-content-wrap realfactory-item-pdlr clearfix"">
                    <div class=""realfactory-content-area"">
                        <article id=""post-1313"" class=""post-1313 post type-post status-publish format-standard has-post-thumbnail hentry category-blog tag-metal tag-mining tag-polymer"">
                            <div class=""realfactory-single-article"">
                                <div class=""realfactory-single-article-content"">
                                    <p>
                                      ");
            WriteLiteral(@"  Fabrikanızın bir gün önceki Toplam Ekipman Etkinliğini ölçümleyebilirsiniz. Dünya standartlarına göre OEE değerinizin trend olup olmadığını öğrenebilirsiniz.
                                        Toplam Ekipman Etkinliği, en basit tanımıyla “kaliteli bir ürün üretmek için zamanın ne kadar etkili kullanıldığının”ölçülmesidir.
                                        OEE’nin genel formülünü aşağıdaki gibi gösterebiliriz: OEE= Kullanılabilir Zaman (Availibility) x Verimlilik (Productivity) x Kalite (Quality)
                                        Bu terim, üretim yapılan makinenin üretim süresince ne kadar süre çalıştığını (kullanılabilirlik) hesaplamak için kullanılır. Bu hesaplamayı yaparken amacımız makinemizin o gün içerisindeki gerçek kullanılabilir zamanını hesaplamaktır.
                                        Eğer doğru kullanılırsa OEE gerçekten yalın üretim için çok verimli bir konsepttir, bu sebeplerden biri iyi analiz edilmiş OEE raporu bize neden üretim süresinde istenilen miktara ulaşamadığ");
            WriteLiteral(@"ımızı, neden çok fazla makina arızası olduğunu ve diğer sebepleri gösterir. Bu bilgiler bize gereken geliştirici planları yapmamız için makine operatörleri, satış elemanları, bakım onarım elemanları için mükemmel bir iletişim imkânı sağlar.
                                    </p>

                                    <div class=""row"">

                                        <div class=""col-lg-6"">
                                            <div id=""contact-form"" class=""form-message text-center"">
                                                <div class=""form-results""></div>
                                                <div class=""input-field animated"" data-animate=""fadeInUp"" data-delay="".4"">
                                                    Parametre girişi yaparak OEE değerinizi hesaplayabilirsiniz.
                                                </div>
                                            </div>



                                            <div id=""contact-form"" class=""form-me");
            WriteLiteral(@"ssage text-center"">
                                                <div class=""form-results""></div>
                                                <div class=""input-field animated"" data-animate=""fadeInUp"" data-delay="".4"">
                                                    <input id=""cevrimZamani"" name=""Name"" type=""number"" class=""input-line required small noscroll"">
                                                    <label class=""input-title "">Çevirim Zamanı (Saniye)</label>
                                                </div>
                                            </div>

                                            <div id=""contact-form"" class=""form-message text-center"">
                                                <div class=""form-results""></div>
                                                <div class=""input-field animated"" data-animate=""fadeInUp"" data-delay="".4"">
                                                    <input id=""uretimAdet"" name=""Name"" type=""number"" class=""input-line");
            WriteLiteral(@" required small noscroll"">
                                                    <label class=""input-title "">Üretim (Adet)</label>
                                                </div>
                                            </div>

                                            <div id=""contact-form"" class=""form-message text-center"">
                                                <div class=""form-results""></div>
                                                <div class=""input-field animated"" data-animate=""fadeInUp"" data-delay="".4"">
                                                    <input id=""hurdaAdet"" name=""Name"" type=""number"" class=""input-line required small"">
                                                    <label class=""input-title "">Hurda (Adet)</label>
                                                </div>
                                            </div>

                                            <div id=""contact-form"" class=""form-message text-center"">
                         ");
            WriteLiteral(@"                       <div class=""form-results""></div>
                                                <div class=""input-field animated"" data-animate=""fadeInUp"" data-delay="".4"">
                                                    <input id=""vardiyaSuresi"" name=""Name"" type=""number"" class=""input-line required small"">
                                                    <label class=""input-title "">Vardiya Süresi (Saat)</label>
                                                </div>
                                            </div>

                                            <div id=""contact-form"" class=""form-message text-center"">
                                                <div class=""form-results""></div>
                                                <div class=""input-field animated"" data-animate=""fadeInUp"" data-delay="".4"">
                                                    <input id=""planliDurus"" name=""Name"" type=""number"" class=""input-line required small"">
                                    ");
            WriteLiteral(@"                <label class=""input-title "">Planlı Duruş (Dakika)</label>
                                                </div>
                                            </div>


                                            <div id=""contact-form"" class=""form-message text-center"">
                                                <div class=""form-results""></div>
                                                <div class=""input-field animated"" data-animate=""fadeInUp"" data-delay="".4"">
                                                    <input id=""plansizDurus"" name=""Name"" type=""number"" class=""input-line required small"">
                                                    <label class=""input-title "">Plansız Duruş (Dakika)</label>
                                                </div>
                                            </div>

                                        </div>
                                        <div class=""col-lg-6"">
                                            <div id=""con");
            WriteLiteral(@"tact-form"" class=""form-message text-center"">
                                                <div class=""form-results""></div>
                                                <div class=""input-field animated"" data-animate=""fadeInUp"" data-delay="".4"">
                                                    Parametrelere Göre Hesaplanan Sonuçlar
                                                </div>
                                            </div>
                                            <div id=""contact-form"" class=""form-message text-center"">
                                                <div class=""form-results""></div>
                                                <div class=""input-field animated"" data-animate=""fadeInUp"" data-delay="".4"">
                                                    <input id=""netKullanilabilirSaat"" name=""Name"" type=""number"" value=""0"" class=""input-line required small"" readonly>
                                                    <label class=""input-title "">Net Kullanılabili");
            WriteLiteral(@"r Saat</label>
                                                </div>
                                            </div>

                                            <div id=""contact-form"" class=""form-message text-center"">
                                                <div class=""form-results""></div>
                                                <div class=""input-field animated"" data-animate=""fadeInUp"" data-delay="".4"">
                                                    <input id=""netOperasyonSaati"" name=""Name"" type=""number"" value=""0"" class=""input-line required small"" readonly>
                                                    <label class=""input-title "">Net Operasyon Saati</label>
                                                </div>
                                            </div>

                                            <div id=""contact-form"" class=""form-message text-center"">
                                                <div class=""form-results""></div>
                        ");
            WriteLiteral(@"                        <div class=""input-field animated"" data-animate=""fadeInUp"" data-delay="".4"">
                                                    <input id=""tumUretimSaati"" name=""Name"" type=""number"" value=""0"" class=""input-line required small"" readonly>
                                                    <label class=""input-title "">Tüm Üretim Saati</label>
                                                </div>
                                            </div>

                                            <div id=""contact-form"" class=""form-message text-center"">
                                                <div class=""form-results""></div>
                                                <div class=""input-field animated"" data-animate=""fadeInUp"" data-delay="".4"">
                                                    <input id=""hurdaKayipSaati"" name=""Name"" type=""number"" value=""0"" class=""input-line required small"" readonly>
                                                    <label class=""input-title """);
            WriteLiteral(@">Hurda Nedeniyle Kayıp Zaman</label>
                                                </div>
                                            </div>



                                            <div id=""contact-form"" class=""form-message text-center"">
                                                <div class=""form-results""></div>
                                                <div class=""input-field animated"" data-animate=""fadeInUp"" data-delay="".4"">
                                                    <input id=""kullanilabilirlik"" name=""Name"" type=""number"" value=""0"" class=""input-line required small"" readonly>
                                                    <label class=""input-title "">Kullanılabilirlik (%)</label>
                                                </div>
                                            </div>


                                            <div id=""contact-form"" class=""form-message text-center"">
                                                <div class=""form-results""></");
            WriteLiteral(@"div>
                                                <div class=""input-field animated"" data-animate=""fadeInUp"" data-delay="".4"">
                                                    <input id=""verimlilik"" name=""Name"" type=""number"" value=""0"" class=""input-line required small"" readonly>
                                                    <label class=""input-title "">Verimlilik (%)</label>
                                                </div>
                                            </div>


                                            <div id=""contact-form"" class=""form-message text-center"">
                                                <div class=""form-results""></div>
                                                <div class=""input-field animated"" data-animate=""fadeInUp"" data-delay="".4"">
                                                    <input id=""kalite"" name=""Name"" type=""number"" value=""0"" class=""input-line required small"" readonly>
                                                    <label cla");
            WriteLiteral(@"ss=""input-title "">Kalite (%)</label>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <br /><br />
                                    <div class=""row text-center"">

                                        <div class=""col-md-4"">


                                            <h6> Toplam Ekipman Etkinliği<br /> OEE ( <label id=""oeelbl""></label> )</h6>

                                        </div>
                                        <div class=""col-md-8"">
                                            <div class=""progress"">
                                                <div id=""progressbar"" class=""progress-bar progress-bar-striped"" role=""progressbar"" style=""width: 0%"" aria-valuenow=""0"" aria-valuemin=""0"" aria-valuemax=""100""></div>
                                            </div>
                            ");
            WriteLiteral(@"            </div>
                                    </div>
                                </div>
                            </div>
                        </article>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

        <script src=""https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js""></script>
        <script src=""/js/Calculations.js""></script>

");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
