#pragma checksum "C:\Users\emre.senturk\source\repos\Kapasitematik\Kapasitematik\Views\Home\Videos.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "c1dfbece6bbf432758db32f70411b72c15b720d5"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Home_Videos), @"mvc.1.0.view", @"/Views/Home/Videos.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "C:\Users\emre.senturk\source\repos\Kapasitematik\Kapasitematik\Views\_ViewImports.cshtml"
using Kapasitematik;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\emre.senturk\source\repos\Kapasitematik\Kapasitematik\Views\_ViewImports.cshtml"
using Kapasitematik.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"c1dfbece6bbf432758db32f70411b72c15b720d5", @"/Views/Home/Videos.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"b047e735b4ff49080d6105bca84291c8dca4e7ba", @"/_ViewImports.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"19ff504f5cbbc4178e156920e4ecf9aef42e51e7", @"/Views/_ViewImports.cshtml")]
    public class Views_Home_Videos : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral("\r\n");
#nullable restore
#line 2 "C:\Users\emre.senturk\source\repos\Kapasitematik\Kapasitematik\Views\Home\Videos.cshtml"
  
    ViewData["Title"] = "Videos";
    Layout = "~/Views/Shared/_LayoutEn.cshtml";

#line default
#line hidden
#nullable disable
            WriteLiteral(@"
<style>
    .video a {
        position: absolute;
        display: block;
        background: url(http://www.slatecube.com/images/play-btn.png) no-repeat center;
        background-size: contain;
        height: 50px;
        width: 80px;
        top: 80px;
        left: 172px;
    }
</style>
<div class=""realfactory-page-wrapper"" id=""realfactory-page-wrapper"">
    <div class=""gdlr-core-page-builder-body"">
        <div class=""gdlr-core-pbf-wrapper "" style=""padding: 150px 30px 50px 30px;"">
            <div class=""gdlr-core-pbf-background-wrap""></div>
            <div class=""gdlr-core-pbf-wrapper-content gdlr-core-js "">
                <div class=""gdlr-core-pbf-wrapper-container clearfix gdlr-core-pbf-wrapper-full"">
                    <div class=""gdlr-core-pbf-element"">
                        <div class=""gdlr-core-portfolio-item gdlr-core-item-pdb clearfix  gdlr-core-portfolio-item-style-grid"" style=""padding-bottom: 10px ;"">
                            <div class=""gdlr-core-portfolio-ite");
            WriteLiteral(@"m-holder gdlr-core-js-2 clearfix"" data-layout=""fitrows"">
                                <div class=""gdlr-core-item-list  gdlr-core-item-pdlr gdlr-core-column-20"">
                                    <div class=""gdlr-core-portfolio-grid  gdlr-core-center-align gdlr-core-style-normal"">
                                        <div class=""gdlr-core-portfolio-thumbnail gdlr-core-media-image  gdlr-core-style-margin-icon"">
                                            <div class=""gdlr-core-portfolio-thumbnail-image-wrap video"">
                                                <img src=""/images/CNC-milling.jpg"" alt=""Fanuc mtconnect"" style=""width:400px; height:200px;"">
                                                <a href=""https://www.youtube.com/watch?v=sqM-MBN3-PA"" class=""play-btn-s2 video-play"">
                                                </a>
                                                <p>Real-Time Count Tracking</p>
                                            </div>
                             ");
            WriteLiteral(@"           </div>
                                    </div>
                                </div>
                                <div class=""gdlr-core-item-list  gdlr-core-item-pdlr gdlr-core-column-20"">
                                    <div class=""gdlr-core-portfolio-grid  gdlr-core-center-align gdlr-core-style-normal"">
                                        <div class=""gdlr-core-portfolio-thumbnail gdlr-core-media-image  gdlr-core-style-margin-icon"">
                                            <div class=""gdlr-core-portfolio-thumbnail-image-wrap video"">
                                                <img src=""/images/CNC-milling.jpg"" alt=""Fanuc mtconnect"" style=""width:400px; height:200px;"">
                                                <a href=""https://www.youtube.com/watch?v=-8eH5KoRbHE"" class=""play-btn-s2 video-play"">
                                                </a>
                                                <p>Operator Performance Tracking</p>
                               ");
            WriteLiteral(@"             </div>
                                        </div>
                                    </div>
                                </div>
                                <div class=""gdlr-core-item-list  gdlr-core-item-pdlr gdlr-core-column-20"">
                                    <div class=""gdlr-core-portfolio-grid  gdlr-core-center-align gdlr-core-style-normal"">
                                        <div class=""gdlr-core-portfolio-thumbnail gdlr-core-media-image  gdlr-core-style-margin-icon"">
                                            <div class=""gdlr-core-portfolio-thumbnail-image-wrap video"">
                                                <img src=""/images/CNC-milling.jpg"" alt=""Fanuc mtconnect"" style=""width:400px; height:200px;"">
                                                <a href=""https://www.youtube.com/watch?v=hJJSoZAYC1E"" class=""play-btn-s2 video-play"">
                                                </a>
                                                <p>Machine Performa");
            WriteLiteral(@"nce Tracking</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class=""gdlr-core-item-list  gdlr-core-item-pdlr gdlr-core-column-20"">
                                    <div class=""gdlr-core-portfolio-grid  gdlr-core-center-align gdlr-core-style-normal"">
                                        <div class=""gdlr-core-portfolio-thumbnail gdlr-core-media-image  gdlr-core-style-margin-icon"">
                                            <div class=""gdlr-core-portfolio-thumbnail-image-wrap video"">
                                                <img src=""/images/CNC-milling.jpg"" alt=""Fanuc mtconnect"" style=""width:400px; height:200px;"">
                                                <a href=""https://www.youtube.com/watch?v=hX7kbVDMfb4"" class=""play-btn-s2 video-play"">
                                                </a>
                  ");
            WriteLiteral(@"                              <p>Program Number Tracking</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class=""gdlr-core-item-list  gdlr-core-item-pdlr gdlr-core-column-20"">
                                    <div class=""gdlr-core-portfolio-grid  gdlr-core-center-align gdlr-core-style-normal"">
                                        <div class=""gdlr-core-portfolio-thumbnail gdlr-core-media-image  gdlr-core-style-margin-icon"">
                                            <div class=""gdlr-core-portfolio-thumbnail-image-wrap video"">
                                                <img src=""/images/CNC-milling.jpg"" alt=""Fanuc mtconnect"" style=""width:400px; height:200px;"">
                                                <a href=""https://www.youtube.com/watch?v=QOjUKcmxrgk"" class=""play-btn-s2 video-play"">
                            ");
            WriteLiteral(@"                    </a>
                                                <p>Shift System</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class=""gdlr-core-item-list  gdlr-core-item-pdlr gdlr-core-column-20"">
                                    <div class=""gdlr-core-portfolio-grid  gdlr-core-center-align gdlr-core-style-normal"">
                                        <div class=""gdlr-core-portfolio-thumbnail gdlr-core-media-image  gdlr-core-style-margin-icon"">
                                            <div class=""gdlr-core-portfolio-thumbnail-image-wrap video"">
                                                <img src=""/images/CNC-milling.jpg"" alt=""Fanuc mtconnect"" style=""width:400px; height:200px;"">
                                                <a href=""https://www.youtube.com/watch?v=U6M00aalAGw"" class=""play-btn-s2 video-pla");
            WriteLiteral(@"y"">
                                                </a>
                                                <p>Total Parça Piece of Count Analysis</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class=""gdlr-core-item-list  gdlr-core-item-pdlr gdlr-core-column-20"">
                                    <div class=""gdlr-core-portfolio-grid  gdlr-core-center-align gdlr-core-style-normal"">
                                        <div class=""gdlr-core-portfolio-thumbnail gdlr-core-media-image  gdlr-core-style-margin-icon"">
                                            <div class=""gdlr-core-portfolio-thumbnail-image-wrap video"">
                                                <img src=""/images/cncstop.jpg"" alt=""Fanuc mtconnect"" style=""width:400px; height:200px;"">
                                                <a href=""https://www.youtube.c");
            WriteLiteral(@"om/watch?v=qm6nHZXeIvg"" class=""play-btn-s2 video-play"">
                                                </a>
                                                <p>OEE Analysis</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class=""gdlr-core-item-list  gdlr-core-item-pdlr gdlr-core-column-20"">
                                    <div class=""gdlr-core-portfolio-grid  gdlr-core-center-align gdlr-core-style-normal"">
                                        <div class=""gdlr-core-portfolio-thumbnail gdlr-core-media-image  gdlr-core-style-margin-icon"">
                                            <div class=""gdlr-core-portfolio-thumbnail-image-wrap video"">
                                                <img src=""/images/cncstop.jpg"" alt=""Fanuc mtconnect"" style=""width:400px; height:200px;"">
                                                <");
            WriteLiteral(@"a href=""https://www.youtube.com/watch?v=IrIqH680c80"" class=""play-btn-s2 video-play"">
                                                </a>
                                                <p>Target Production Analysis</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class=""gdlr-core-item-list  gdlr-core-item-pdlr gdlr-core-column-20"">
                                    <div class=""gdlr-core-portfolio-grid  gdlr-core-center-align gdlr-core-style-normal"">
                                        <div class=""gdlr-core-portfolio-thumbnail gdlr-core-media-image  gdlr-core-style-margin-icon"">
                                            <div class=""gdlr-core-portfolio-thumbnail-image-wrap video"">
                                                <img src=""/images/cncstop.jpg"" alt=""Fanuc mtconnect"" style=""width:400px; height:200px;"">
      ");
            WriteLiteral(@"                                          <a href=""https://www.youtube.com/watch?v=rHHNT7v5I1c"" class=""play-btn-s2 video-play"">
                                                </a>
                                                <p>Distrupt Notification Tracing</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class=""gdlr-core-item-list  gdlr-core-item-pdlr gdlr-core-column-20"">
                                    <div class=""gdlr-core-portfolio-grid  gdlr-core-center-align gdlr-core-style-normal"">
                                        <div class=""gdlr-core-portfolio-thumbnail gdlr-core-media-image  gdlr-core-style-margin-icon"">
                                            <div class=""gdlr-core-portfolio-thumbnail-image-wrap video"">
                                                <img src=""/images/cncstop.jpg"" alt=""Fanuc mtconnec");
            WriteLiteral(@"t"" style=""width:400px; height:200px;"">
                                                <a href=""https://www.youtube.com/watch?v=n3mj3TdnWRw"" class=""play-btn-s2 video-play"">
                                                </a>
                                                <p>Production Line</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class=""gdlr-core-item-list  gdlr-core-item-pdlr gdlr-core-column-20"">
                                    <div class=""gdlr-core-portfolio-grid  gdlr-core-center-align gdlr-core-style-normal"">
                                        <div class=""gdlr-core-portfolio-thumbnail gdlr-core-media-image  gdlr-core-style-margin-icon"">
                                            <div class=""gdlr-core-portfolio-thumbnail-image-wrap video"">
                                                <img src=""/images/");
            WriteLiteral(@"cncstop.jpg"" alt=""Fanuc mtconnect"" style=""width:400px; height:200px;"">
                                                <a href=""https://www.youtube.com/watch?v=br0NQxoJ-WE"" class=""play-btn-s2 video-play"">
                                                </a>
                                                <p>Operation Volume</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class=""gdlr-core-item-list  gdlr-core-item-pdlr gdlr-core-column-20"">
                                    <div class=""gdlr-core-portfolio-grid  gdlr-core-center-align gdlr-core-style-normal"">
                                        <div class=""gdlr-core-portfolio-thumbnail gdlr-core-media-image  gdlr-core-style-margin-icon"">
                                            <div class=""gdlr-core-portfolio-thumbnail-image-wrap video"">
                                 ");
            WriteLiteral(@"               <img src=""/images/cncstop.jpg"" alt=""Fanuc mtconnect"" style=""width:400px; height:200px;"">
                                                <a href=""https://www.youtube.com/watch?v=b4Ok76zhxyY"" class=""play-btn-s2 video-play"">
                                                </a>
                                                <p>Total Daily Business Volume</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
