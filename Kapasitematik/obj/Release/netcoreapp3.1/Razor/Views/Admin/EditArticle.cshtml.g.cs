#pragma checksum "C:\Users\emre.senturk\source\repos\Kapasitematik\Kapasitematik\Views\Admin\EditArticle.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "5656c1359a7e9293cd0910ab7964daf32e03b017"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Admin_EditArticle), @"mvc.1.0.view", @"/Views/Admin/EditArticle.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "C:\Users\emre.senturk\source\repos\Kapasitematik\Kapasitematik\Views\_ViewImports.cshtml"
using Kapasitematik;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\emre.senturk\source\repos\Kapasitematik\Kapasitematik\Views\_ViewImports.cshtml"
using Kapasitematik.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"5656c1359a7e9293cd0910ab7964daf32e03b017", @"/Views/Admin/EditArticle.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"b047e735b4ff49080d6105bca84291c8dca4e7ba", @"/_ViewImports.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"19ff504f5cbbc4178e156920e4ecf9aef42e51e7", @"/Views/_ViewImports.cshtml")]
    public class Views_Admin_EditArticle : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<Kapasitematik.Database.Article>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("class", new global::Microsoft.AspNetCore.Html.HtmlString("form-validate form-horizontal"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("id", new global::Microsoft.AspNetCore.Html.HtmlString("feedback_form"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_2 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("method", "post", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_3 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-controller", "Admin", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_4 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "EditArticle", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_5 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("enctype", new global::Microsoft.AspNetCore.Html.HtmlString("multipart/form-data"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_6 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("novalidate", new global::Microsoft.AspNetCore.Html.HtmlString("novalidate"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        #pragma warning restore 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper;
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.RenderAtEndOfFormTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#nullable restore
#line 2 "C:\Users\emre.senturk\source\repos\Kapasitematik\Kapasitematik\Views\Admin\EditArticle.cshtml"
  
    ViewData["Title"] = "Makale Düzenle";
    Layout = "~/Views/Shared/_LayoutAdmin.cshtml";

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n");
            WriteLiteral("\r\n<div class=\"form\">\r\n    ");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("form", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "5656c1359a7e9293cd0910ab7964daf32e03b0176136", async() => {
                WriteLiteral(@"
        <div class=""form-group "">
            <label for=""cname"" class=""control-label col-lg-2"" hidden>Id <span class=""required"">*</span></label>
            <div class=""col-lg-10"">
                <input class=""form-control"" id=""Id"" name=""Id"" minlength=""5"" type=""hidden""");
                BeginWriteAttribute("required", " required=\"", 657, "\"", 668, 0);
                EndWriteAttribute();
                BeginWriteAttribute("value", " value=\"", 669, "\"", 686, 1);
#nullable restore
#line 14 "C:\Users\emre.senturk\source\repos\Kapasitematik\Kapasitematik\Views\Admin\EditArticle.cshtml"
WriteAttributeValue("", 677, Model.Id, 677, 9, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral(@">
            </div>
        </div>
        <div class=""form-group "">
            <label for=""cname"" class=""control-label col-lg-2"">Başlık <span class=""required"">*</span></label>
            <div class=""col-lg-10"">
                <input class=""form-control"" id=""Baslik"" name=""Baslik"" minlength=""5"" type=""text""");
                BeginWriteAttribute("required", " required=\"", 1003, "\"", 1014, 0);
                EndWriteAttribute();
                BeginWriteAttribute("value", " value=\"", 1015, "\"", 1036, 1);
#nullable restore
#line 20 "C:\Users\emre.senturk\source\repos\Kapasitematik\Kapasitematik\Views\Admin\EditArticle.cshtml"
WriteAttributeValue("", 1023, Model.Baslik, 1023, 13, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral(@">
            </div>
        </div>
        <div class=""form-group "">
            <label for=""cemail"" class=""control-label col-lg-2"">İçerik <span class=""required"">*</span></label>
            <div class=""col-lg-10"">
                <textarea class=""ckeditor"" name=""Icerik"" id=""Icerik"">");
#nullable restore
#line 26 "C:\Users\emre.senturk\source\repos\Kapasitematik\Kapasitematik\Views\Admin\EditArticle.cshtml"
                                                                Write(Model.Icerik);

#line default
#line hidden
#nullable disable
                WriteLiteral(@"</textarea>
            </div>
        </div>
        <div class=""form-group "">
            <label for=""cname"" class=""control-label col-lg-2"">Önceki Resim <span class=""required"">*</span></label>
            <div class=""col-lg-10"">
                <img");
                BeginWriteAttribute("src", " src=\"", 1598, "\"", 1619, 1);
#nullable restore
#line 32 "C:\Users\emre.senturk\source\repos\Kapasitematik\Kapasitematik\Views\Admin\EditArticle.cshtml"
WriteAttributeValue("", 1604, Model.ResimURL, 1604, 15, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral(@" width=""150"" height=""100"" />
            </div>
        </div>
        <div class=""form-group "">
            <label for=""cname"" class=""control-label col-lg-2"">Resim <span class=""required"">*</span></label>
            <div class=""col-lg-10"">
                <input class=""form-control"" id=""ResimURL"" name=""ResimURL"" minlength=""5"" type=""file"" accept="".jpg,.png,.jpeg"">
            </div>
        </div>
        <div class=""form-group "">
            <label for=""cname"" class=""control-label col-lg-2"">Tarih <span class=""required"">*</span></label>
            <div class=""col-lg-10"">
                <input name=""Tarih"" id=""Tarih"" type=""text"" class=""form-control""");
                BeginWriteAttribute("value", " value=\"", 2290, "\"", 2330, 1);
#nullable restore
#line 44 "C:\Users\emre.senturk\source\repos\Kapasitematik\Kapasitematik\Views\Admin\EditArticle.cshtml"
WriteAttributeValue("", 2298, Model.Tarih.ToShortDateString(), 2298, 32, false);

#line default
#line hidden
#nullable disable
                EndWriteAttribute();
                WriteLiteral(@">
            </div>
        </div>
        <div class=""form-group"">
            <div class=""col-lg-offset-2 col-lg-10"">
                <button class=""btn btn-primary"" type=""submit"">Düzenle</button>
                <button class=""btn btn-default"" type=""button""><a href=""/makale-listesi"">İptal</a></button>
            </div>
        </div>
    ");
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.RenderAtEndOfFormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_0);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_1);
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper.Method = (string)__tagHelperAttribute_2.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_2);
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper.Controller = (string)__tagHelperAttribute_3.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_3);
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper.Action = (string)__tagHelperAttribute_4.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_4);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_5);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_6);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\r\n\r\n");
#nullable restore
#line 55 "C:\Users\emre.senturk\source\repos\Kapasitematik\Kapasitematik\Views\Admin\EditArticle.cshtml"
     if (TempData["mesaj"] != null)
    {

#line default
#line hidden
#nullable disable
            WriteLiteral("        <div id=\"dialog\" title=\"Bilgi Mesajı\">\r\n            <p>\r\n                ");
#nullable restore
#line 59 "C:\Users\emre.senturk\source\repos\Kapasitematik\Kapasitematik\Views\Admin\EditArticle.cshtml"
           Write(TempData["mesaj"].ToString());

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n            </p>\r\n        </div>\r\n");
#nullable restore
#line 62 "C:\Users\emre.senturk\source\repos\Kapasitematik\Kapasitematik\Views\Admin\EditArticle.cshtml"
    }

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n</div>\r\n\r\n");
            DefineSection("Scripts", async() => {
                WriteLiteral(@"
    <script src=""//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.10.0/jquery-ui.js""></script>
    <script>
        $(""#dialog"").dialog();
        $(""#Tarih"").datepicker({
            dateFormat: ""dd/mm/yy"",
            onSelect: function (date) {
                minDateFilter = new Date(date).getTime();
            }
        });
    </script>
");
            }
            );
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<Kapasitematik.Database.Article> Html { get; private set; }
    }
}
#pragma warning restore 1591
