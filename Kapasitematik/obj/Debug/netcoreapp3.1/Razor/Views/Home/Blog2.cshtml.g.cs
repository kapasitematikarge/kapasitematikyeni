#pragma checksum "C:\Users\emre.senturk\source\repos\Kapasitematik\Kapasitematik\Views\Home\Blog2.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "f9d088deaa385a72cae875b8b2a86b141c6852a7"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Home_Blog2), @"mvc.1.0.view", @"/Views/Home/Blog2.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "C:\Users\emre.senturk\source\repos\Kapasitematik\Kapasitematik\Views\_ViewImports.cshtml"
using Kapasitematik;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\emre.senturk\source\repos\Kapasitematik\Kapasitematik\Views\_ViewImports.cshtml"
using Kapasitematik.Models;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\emre.senturk\source\repos\Kapasitematik\Kapasitematik\Views\Home\Blog2.cshtml"
using PagedList.Core.Mvc;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"f9d088deaa385a72cae875b8b2a86b141c6852a7", @"/Views/Home/Blog2.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"b047e735b4ff49080d6105bca84291c8dca4e7ba", @"/_ViewImports.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"19ff504f5cbbc4178e156920e4ecf9aef42e51e7", @"/Views/_ViewImports.cshtml")]
    public class Views_Home_Blog2 : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<ViewModel>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#nullable restore
#line 3 "C:\Users\emre.senturk\source\repos\Kapasitematik\Kapasitematik\Views\Home\Blog2.cshtml"
  
    ViewData["Title"] = "Blog";
    Layout = "~/Views/Shared/_Layout.cshtml";

#line default
#line hidden
#nullable disable
            WriteLiteral(@"<link rel=""stylesheet"" href=""https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css"">
<script src=""https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js""></script>
<script src=""https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js""></script>

");
            WriteLiteral(@"        <div class=""gdlr-core-pbf-section"">
            <div class=""gdlr-core-pbf-section-container gdlr-core-container clearfix"">
                <div class=""gdlr-core-pbf-element"">
                    <br /><br /><br /><br />
                                      <div class=""gdlr-core-blog-item gdlr-core-item-pdb clearfix  gdlr-core-style-blog-column"" style=""padding-bottom: 40px ;"">
                                          <div class=""gdlr-core-blog-item-holder gdlr-core-js-2 clearfix"" data-layout=""fitrows"">
");
#nullable restore
#line 19 "C:\Users\emre.senturk\source\repos\Kapasitematik\Kapasitematik\Views\Home\Blog2.cshtml"
                                               foreach (var item in Model.OneArticle)
                                              {

#line default
#line hidden
#nullable disable
            WriteLiteral(@"                                                  <div class=""gdlr-core-item-list  gdlr-core-item-pdlr gdlr-core-column-30"">
                                                      <div class=""gdlr-core-blog-grid "">
                                                          <div class=""gdlr-core-blog-thumbnail gdlr-core-media-image  gdlr-core-opacity-on-hover gdlr-core-zoom-on-hover"">
                                                              <a");
            BeginWriteAttribute("href", " href=\"", 1653, "\"", 1684, 2);
            WriteAttributeValue("", 1660, "/Home/BlogDetay/", 1660, 16, true);
#nullable restore
#line 24 "C:\Users\emre.senturk\source\repos\Kapasitematik\Kapasitematik\Views\Home\Blog2.cshtml"
WriteAttributeValue("", 1676, item.Id, 1676, 8, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(">\r\n                                                                  <img");
            BeginWriteAttribute("src", " src=\"", 1758, "\"", 1778, 1);
#nullable restore
#line 25 "C:\Users\emre.senturk\source\repos\Kapasitematik\Kapasitematik\Views\Home\Blog2.cshtml"
WriteAttributeValue("", 1764, item.ResimUrl, 1764, 14, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(" width=\"800\" height=\"570\"");
            BeginWriteAttribute("srcset", " srcset=\"", 1804, "\"", 1874, 6);
#nullable restore
#line 25 "C:\Users\emre.senturk\source\repos\Kapasitematik\Kapasitematik\Views\Home\Blog2.cshtml"
WriteAttributeValue("", 1813, item.ResimUrl, 1813, 14, false);

#line default
#line hidden
#nullable disable
            WriteAttributeValue(" ", 1827, "400w,", 1828, 6, true);
#nullable restore
#line 25 "C:\Users\emre.senturk\source\repos\Kapasitematik\Kapasitematik\Views\Home\Blog2.cshtml"
WriteAttributeValue(" ", 1833, item.ResimUrl, 1834, 14, false);

#line default
#line hidden
#nullable disable
            WriteAttributeValue(" ", 1848, "600w,", 1849, 6, true);
#nullable restore
#line 25 "C:\Users\emre.senturk\source\repos\Kapasitematik\Kapasitematik\Views\Home\Blog2.cshtml"
WriteAttributeValue(" ", 1854, item.ResimUrl, 1855, 14, false);

#line default
#line hidden
#nullable disable
            WriteAttributeValue(" ", 1869, "800w", 1870, 5, true);
            EndWriteAttribute();
            WriteLiteral(" sizes=\"(max-width: 767px) 100vw, (max-width: 1150px) 33vw, 383px\"");
            BeginWriteAttribute("alt", " alt=\"", 1941, "\"", 1947, 0);
            EndWriteAttribute();
            WriteLiteral(" style=\"height:486px;\">\r\n                                                                  <div class=\"gdlr-core-sticky-banner gdlr-core-title-font\"><i class=\"fa fa-bolt\"></i>");
#nullable restore
#line 26 "C:\Users\emre.senturk\source\repos\Kapasitematik\Kapasitematik\Views\Home\Blog2.cshtml"
                                                                                                                                                 Write(item.KategoriAdi);

#line default
#line hidden
#nullable disable
            WriteLiteral(@"</div>
                                                              </a>
                                                          </div>
                                                          <div class=""gdlr-core-blog-grid-content-wrap"">
                                                              <div class=""gdlr-core-blog-info-wrapper gdlr-core-skin-divider"">
                                                                  <span class=""gdlr-core-blog-info gdlr-core-blog-info-font gdlr-core-skin-caption gdlr-core-blog-info-date"">
                                                                      <span class=""gdlr-core-head"">
                                                                          <i class=""fa fa-clock-o""></i>
                                                                      </span>
                                                                      <a href=""#"">");
#nullable restore
#line 35 "C:\Users\emre.senturk\source\repos\Kapasitematik\Kapasitematik\Views\Home\Blog2.cshtml"
                                                                             Write(item.Tarih.ToShortDateString());

#line default
#line hidden
#nullable disable
            WriteLiteral(@"</a>
                                                                  </span>
                                                              </div>
                                                              <h3 class=""gdlr-core-blog-title gdlr-core-skin-title"" style=""font-size: 22px ;font-weight: 500 ;letter-spacing: 0px ;"">
                                                                  <a");
            BeginWriteAttribute("href", " href=\"", 3491, "\"", 3522, 2);
            WriteAttributeValue("", 3498, "/Home/BlogDetay/", 3498, 16, true);
#nullable restore
#line 39 "C:\Users\emre.senturk\source\repos\Kapasitematik\Kapasitematik\Views\Home\Blog2.cshtml"
WriteAttributeValue("", 3514, item.Id, 3514, 8, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(">");
#nullable restore
#line 39 "C:\Users\emre.senturk\source\repos\Kapasitematik\Kapasitematik\Views\Home\Blog2.cshtml"
                                                                                                Write(item.Baslik);

#line default
#line hidden
#nullable disable
            WriteLiteral("</a>\r\n                                                              </h3>\r\n");
            WriteLiteral("                                                          </div>\r\n                                                      </div>\r\n                                                  </div>\r\n");
#nullable restore
#line 52 "C:\Users\emre.senturk\source\repos\Kapasitematik\Kapasitematik\Views\Home\Blog2.cshtml"
                                              }

#line default
#line hidden
#nullable disable
#nullable restore
#line 53 "C:\Users\emre.senturk\source\repos\Kapasitematik\Kapasitematik\Views\Home\Blog2.cshtml"
                                               foreach (var item in Model.TwoArticle)
                                              {

#line default
#line hidden
#nullable disable
            WriteLiteral(@"                                                  <div class=""gdlr-core-item-list  gdlr-core-item-pdlr gdlr-core-column-15 "">
                                                      <div class=""gdlr-core-blog-grid "">
                                                          <div class=""gdlr-core-blog-thumbnail gdlr-core-media-image  gdlr-core-opacity-on-hover gdlr-core-zoom-on-hover"">
                                                              <a");
            BeginWriteAttribute("href", " href=\"", 4989, "\"", 5020, 2);
            WriteAttributeValue("", 4996, "/Home/BlogDetay/", 4996, 16, true);
#nullable restore
#line 58 "C:\Users\emre.senturk\source\repos\Kapasitematik\Kapasitematik\Views\Home\Blog2.cshtml"
WriteAttributeValue("", 5012, item.Id, 5012, 8, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(">\r\n                                                                  <img");
            BeginWriteAttribute("src", " src=\"", 5094, "\"", 5114, 1);
#nullable restore
#line 59 "C:\Users\emre.senturk\source\repos\Kapasitematik\Kapasitematik\Views\Home\Blog2.cshtml"
WriteAttributeValue("", 5100, item.ResimUrl, 5100, 14, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(" width=\"600\" height=\"750\"");
            BeginWriteAttribute("srcset", " srcset=\"", 5140, "\"", 5189, 4);
#nullable restore
#line 59 "C:\Users\emre.senturk\source\repos\Kapasitematik\Kapasitematik\Views\Home\Blog2.cshtml"
WriteAttributeValue("", 5149, item.ResimUrl, 5149, 14, false);

#line default
#line hidden
#nullable disable
            WriteAttributeValue(" ", 5163, "400w,", 5164, 6, true);
#nullable restore
#line 59 "C:\Users\emre.senturk\source\repos\Kapasitematik\Kapasitematik\Views\Home\Blog2.cshtml"
WriteAttributeValue(" ", 5169, item.ResimUrl, 5170, 14, false);

#line default
#line hidden
#nullable disable
            WriteAttributeValue(" ", 5184, "600w", 5185, 5, true);
            EndWriteAttribute();
            WriteLiteral(" sizes=\"(max-width: 767px) 100vw, (max-width: 1150px) 25vw, 287px\"");
            BeginWriteAttribute("alt", " alt=\"", 5256, "\"", 5262, 0);
            EndWriteAttribute();
            WriteLiteral(" style=\"width: 368px; height: 163px;\">\r\n                                                                  <div class=\"gdlr-core-sticky-banner gdlr-core-title-font\"><i class=\"fa fa-bolt\"></i>");
#nullable restore
#line 60 "C:\Users\emre.senturk\source\repos\Kapasitematik\Kapasitematik\Views\Home\Blog2.cshtml"
                                                                                                                                                 Write(item.KategoriAdi);

#line default
#line hidden
#nullable disable
            WriteLiteral(@"</div>
                                                              </a>
                                                          </div>
                                                          <div class=""gdlr-core-blog-grid-content-wrap"">
                                                              <div class=""gdlr-core-blog-info-wrapper gdlr-core-skin-divider"">
                                                                  <span class=""gdlr-core-blog-info gdlr-core-blog-info-font gdlr-core-skin-caption gdlr-core-blog-info-date"">
                                                                      <span class=""gdlr-core-head"">
                                                                          <i class=""fa fa-clock-o""></i>
                                                                      </span>
                                                                      <a href=""#"">");
#nullable restore
#line 69 "C:\Users\emre.senturk\source\repos\Kapasitematik\Kapasitematik\Views\Home\Blog2.cshtml"
                                                                             Write(item.Tarih.ToShortDateString());

#line default
#line hidden
#nullable disable
            WriteLiteral(@"</a>
                                                                  </span>
                                                              </div>
                                                              <h3 class=""gdlr-core-blog-title gdlr-core-skin-title"" style=""font-size: 22px ;font-weight: 500 ;letter-spacing: 0px ;"">
                                                                  <a");
            BeginWriteAttribute("href", " href=\"", 6821, "\"", 6852, 2);
            WriteAttributeValue("", 6828, "/Home/BlogDetay/", 6828, 16, true);
#nullable restore
#line 73 "C:\Users\emre.senturk\source\repos\Kapasitematik\Kapasitematik\Views\Home\Blog2.cshtml"
WriteAttributeValue("", 6844, item.Id, 6844, 8, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(">");
#nullable restore
#line 73 "C:\Users\emre.senturk\source\repos\Kapasitematik\Kapasitematik\Views\Home\Blog2.cshtml"
                                                                                                Write(item.Baslik);

#line default
#line hidden
#nullable disable
            WriteLiteral(@"</a>
                                                              </h3>
                                                          </div>
                                                      </div>
                                                  </div>
");
#nullable restore
#line 78 "C:\Users\emre.senturk\source\repos\Kapasitematik\Kapasitematik\Views\Home\Blog2.cshtml"
                                              }

#line default
#line hidden
#nullable disable
#nullable restore
#line 79 "C:\Users\emre.senturk\source\repos\Kapasitematik\Kapasitematik\Views\Home\Blog2.cshtml"
                                               foreach (var item in Model.ArticleSkipTwo)
                                              {

#line default
#line hidden
#nullable disable
            WriteLiteral(@"                                                  <div class=""gdlr-core-item-list  gdlr-core-item-pdlr gdlr-core-column-15"">
                                                      <div class=""gdlr-core-blog-grid "">
                                                          <div class=""gdlr-core-blog-thumbnail gdlr-core-media-image  gdlr-core-opacity-on-hover gdlr-core-zoom-on-hover"">
                                                              <a");
            BeginWriteAttribute("href", " href=\"", 7768, "\"", 7799, 2);
            WriteAttributeValue("", 7775, "/Home/BlogDetay/", 7775, 16, true);
#nullable restore
#line 84 "C:\Users\emre.senturk\source\repos\Kapasitematik\Kapasitematik\Views\Home\Blog2.cshtml"
WriteAttributeValue("", 7791, item.Id, 7791, 8, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(">\r\n                                                                  <img");
            BeginWriteAttribute("src", " src=\"", 7873, "\"", 7893, 1);
#nullable restore
#line 85 "C:\Users\emre.senturk\source\repos\Kapasitematik\Kapasitematik\Views\Home\Blog2.cshtml"
WriteAttributeValue("", 7879, item.ResimUrl, 7879, 14, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(" width=\"600\" height=\"750\"");
            BeginWriteAttribute("srcset", " srcset=\"", 7919, "\"", 7968, 4);
#nullable restore
#line 85 "C:\Users\emre.senturk\source\repos\Kapasitematik\Kapasitematik\Views\Home\Blog2.cshtml"
WriteAttributeValue("", 7928, item.ResimUrl, 7928, 14, false);

#line default
#line hidden
#nullable disable
            WriteAttributeValue(" ", 7942, "400w,", 7943, 6, true);
#nullable restore
#line 85 "C:\Users\emre.senturk\source\repos\Kapasitematik\Kapasitematik\Views\Home\Blog2.cshtml"
WriteAttributeValue(" ", 7948, item.ResimUrl, 7949, 14, false);

#line default
#line hidden
#nullable disable
            WriteAttributeValue(" ", 7963, "600w", 7964, 5, true);
            EndWriteAttribute();
            WriteLiteral(" sizes=\"(max-width: 767px) 100vw, (max-width: 1150px) 25vw, 287px\"");
            BeginWriteAttribute("alt", " alt=\"", 8035, "\"", 8041, 0);
            EndWriteAttribute();
            WriteLiteral(" style=\"width: 368px; height: 163px;\">\r\n                                                                  <div class=\"gdlr-core-sticky-banner gdlr-core-title-font\"><i class=\"fa fa-bolt\"></i>");
#nullable restore
#line 86 "C:\Users\emre.senturk\source\repos\Kapasitematik\Kapasitematik\Views\Home\Blog2.cshtml"
                                                                                                                                                 Write(item.KategoriAdi);

#line default
#line hidden
#nullable disable
            WriteLiteral(@"</div>
                                                              </a>
                                                          </div>
                                                          <div class=""gdlr-core-blog-grid-content-wrap"">
                                                              <div class=""gdlr-core-blog-info-wrapper gdlr-core-skin-divider"">
                                                                  <span class=""gdlr-core-blog-info gdlr-core-blog-info-font gdlr-core-skin-caption gdlr-core-blog-info-date"">
                                                                      <span class=""gdlr-core-head"">
                                                                          <i class=""fa fa-clock-o""></i>
                                                                      </span>
                                                                      <a href=""#"">");
#nullable restore
#line 95 "C:\Users\emre.senturk\source\repos\Kapasitematik\Kapasitematik\Views\Home\Blog2.cshtml"
                                                                             Write(item.Tarih.ToShortDateString());

#line default
#line hidden
#nullable disable
            WriteLiteral(@"</a>
                                                                  </span>
                                                              </div>
                                                              <h3 class=""gdlr-core-blog-title gdlr-core-skin-title"" style=""font-size: 22px ;font-weight: 500 ;letter-spacing: 0px ;"">
                                                                  <a");
            BeginWriteAttribute("href", " href=\"", 9600, "\"", 9631, 2);
            WriteAttributeValue("", 9607, "/Home/BlogDetay/", 9607, 16, true);
#nullable restore
#line 99 "C:\Users\emre.senturk\source\repos\Kapasitematik\Kapasitematik\Views\Home\Blog2.cshtml"
WriteAttributeValue("", 9623, item.Id, 9623, 8, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(">");
#nullable restore
#line 99 "C:\Users\emre.senturk\source\repos\Kapasitematik\Kapasitematik\Views\Home\Blog2.cshtml"
                                                                                                Write(item.Baslik);

#line default
#line hidden
#nullable disable
            WriteLiteral(@"</a>
                                                              </h3>
                                                          </div>
                                                      </div>
                                                  </div>
");
#nullable restore
#line 104 "C:\Users\emre.senturk\source\repos\Kapasitematik\Kapasitematik\Views\Home\Blog2.cshtml"
                                              }

#line default
#line hidden
#nullable disable
            WriteLiteral(@"                                          </div>
                                          <div class=""gdlr-core-title-item-title-wrap "">
                                            <h3 class=""gdlr-core-title-item-title gdlr-core-skin-title "" style=""font-size: 22px ;font-weight: 600 ;"">
                                                Öne Çıkan Makaleler
                                                <span class=""gdlr-core-title-item-title-divider gdlr-core-skin-divider""></span>
                                            </h3>
                                          </div>
                                          <div class=""gdlr-core-blog-item gdlr-core-item-pdb clearfix  gdlr-core-style-blog-column"" style=""padding-bottom: 40px ;"">
                                              <div class=""gdlr-core-blog-item-holder gdlr-core-js-2 clearfix"" data-layout=""fitrows"">
");
#nullable restore
#line 114 "C:\Users\emre.senturk\source\repos\Kapasitematik\Kapasitematik\Views\Home\Blog2.cshtml"
                                                   foreach (var item in Model.Article)
                                                  {

#line default
#line hidden
#nullable disable
            WriteLiteral(@"                                                      <div class=""gdlr-core-item-list  gdlr-core-item-pdlr gdlr-core-column-20 "">
                                                          <div class=""gdlr-core-blog-grid "">
                                                              <div class=""gdlr-core-blog-thumbnail gdlr-core-media-image  gdlr-core-opacity-on-hover gdlr-core-zoom-on-hover"">
                                                                  <a");
            BeginWriteAttribute("href", " href=\"", 11454, "\"", 11485, 2);
            WriteAttributeValue("", 11461, "/Home/BlogDetay/", 11461, 16, true);
#nullable restore
#line 119 "C:\Users\emre.senturk\source\repos\Kapasitematik\Kapasitematik\Views\Home\Blog2.cshtml"
WriteAttributeValue("", 11477, item.Id, 11477, 8, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(">\r\n                                                                      <img");
            BeginWriteAttribute("src", " src=\"", 11563, "\"", 11583, 1);
#nullable restore
#line 120 "C:\Users\emre.senturk\source\repos\Kapasitematik\Kapasitematik\Views\Home\Blog2.cshtml"
WriteAttributeValue("", 11569, item.ResimUrl, 11569, 14, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(" width=\"800\" height=\"570\"");
            BeginWriteAttribute("srcset", " srcset=\"", 11609, "\"", 11679, 6);
#nullable restore
#line 120 "C:\Users\emre.senturk\source\repos\Kapasitematik\Kapasitematik\Views\Home\Blog2.cshtml"
WriteAttributeValue("", 11618, item.ResimUrl, 11618, 14, false);

#line default
#line hidden
#nullable disable
            WriteAttributeValue(" ", 11632, "400w,", 11633, 6, true);
#nullable restore
#line 120 "C:\Users\emre.senturk\source\repos\Kapasitematik\Kapasitematik\Views\Home\Blog2.cshtml"
WriteAttributeValue(" ", 11638, item.ResimUrl, 11639, 14, false);

#line default
#line hidden
#nullable disable
            WriteAttributeValue(" ", 11653, "600w,", 11654, 6, true);
#nullable restore
#line 120 "C:\Users\emre.senturk\source\repos\Kapasitematik\Kapasitematik\Views\Home\Blog2.cshtml"
WriteAttributeValue(" ", 11659, item.ResimUrl, 11660, 14, false);

#line default
#line hidden
#nullable disable
            WriteAttributeValue(" ", 11674, "800w", 11675, 5, true);
            EndWriteAttribute();
            WriteLiteral(" sizes=\"(max-width: 767px) 100vw, (max-width: 1150px) 33vw, 383px\"");
            BeginWriteAttribute("alt", " alt=\"", 11746, "\"", 11752, 0);
            EndWriteAttribute();
            WriteLiteral(" style=\"height:251.74px;\">\r\n                                                                      <div class=\"gdlr-core-sticky-banner gdlr-core-title-font\"><i class=\"fa fa-bolt\"></i>");
#nullable restore
#line 121 "C:\Users\emre.senturk\source\repos\Kapasitematik\Kapasitematik\Views\Home\Blog2.cshtml"
                                                                                                                                                     Write(item.KategoriAdi);

#line default
#line hidden
#nullable disable
            WriteLiteral(@"</div>
                                                                  </a>
                                                              </div>
                                                              <div class=""gdlr-core-blog-grid-content-wrap"">
                                                                  <div class=""gdlr-core-blog-info-wrapper gdlr-core-skin-divider"">
                                                                      <span class=""gdlr-core-blog-info gdlr-core-blog-info-font gdlr-core-skin-caption gdlr-core-blog-info-date"">
                                                                          <span class=""gdlr-core-head"">
                                                                              <i class=""fa fa-clock-o""></i>
                                                                          </span>
                                                                          <a href=""#"">");
#nullable restore
#line 130 "C:\Users\emre.senturk\source\repos\Kapasitematik\Kapasitematik\Views\Home\Blog2.cshtml"
                                                                                 Write(item.Tarih.ToShortDateString());

#line default
#line hidden
#nullable disable
            WriteLiteral("</a>\r\n                                                                      </span>\r\n");
            WriteLiteral(@"                                                                  </div>
                                                                  <h3 class=""gdlr-core-blog-title gdlr-core-skin-title"" style=""font-size: 22px ;font-weight: 500 ;letter-spacing: 0px ;"">
                                                                      <a");
            BeginWriteAttribute("href", " href=\"", 13950, "\"", 13981, 2);
            WriteAttributeValue("", 13957, "/Home/BlogDetay/", 13957, 16, true);
#nullable restore
#line 140 "C:\Users\emre.senturk\source\repos\Kapasitematik\Kapasitematik\Views\Home\Blog2.cshtml"
WriteAttributeValue("", 13973, item.Id, 13973, 8, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(">");
#nullable restore
#line 140 "C:\Users\emre.senturk\source\repos\Kapasitematik\Kapasitematik\Views\Home\Blog2.cshtml"
                                                                                                    Write(item.Baslik);

#line default
#line hidden
#nullable disable
            WriteLiteral(@"</a>
                                                                  </h3>
                                                                  <div class=""gdlr-core-blog-content clearfix"">
                                                                      ");
#nullable restore
#line 143 "C:\Users\emre.senturk\source\repos\Kapasitematik\Kapasitematik\Views\Home\Blog2.cshtml"
                                                                 Write(Html.Raw(item.Icerik.Substring(0, 220)));

#line default
#line hidden
#nullable disable
            WriteLiteral("...\r\n                                                                      <div class=\"clear\"></div>\r\n                                                                      <a class=\"gdlr-core-excerpt-read-more\"");
            BeginWriteAttribute("href", " href=\"", 14507, "\"", 14538, 2);
            WriteAttributeValue("", 14514, "/Home/BlogDetay/", 14514, 16, true);
#nullable restore
#line 145 "C:\Users\emre.senturk\source\repos\Kapasitematik\Kapasitematik\Views\Home\Blog2.cshtml"
WriteAttributeValue("", 14530, item.Id, 14530, 8, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(@">
                                                                          Devamını Oku
                                                                          <i class=""fa fa-long-arrow-right""></i>
                                                                      </a>
                                                                  </div>
                                                              </div>
                                                          </div>
                                                      </div>
");
#nullable restore
#line 153 "C:\Users\emre.senturk\source\repos\Kapasitematik\Kapasitematik\Views\Home\Blog2.cshtml"
                                                  }

#line default
#line hidden
#nullable disable
            WriteLiteral("                                              </div>\r\n");
            WriteLiteral("                                          </div>\r\n                                      </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n");
            WriteLiteral("\r\n\r\n");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<ViewModel> Html { get; private set; }
    }
}
#pragma warning restore 1591
